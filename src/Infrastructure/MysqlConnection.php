<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Infrastructure;

use PDO;
use PDOException;

final class MysqlConnection
{
    public static function create(): PDO
    {
        $dsn = 'mysql:host=mysql;port=3306;dbname=coffee_machine;charset=utf8mb4';
        $user = 'coffee_machine';
        $password = 'coffee_machine';

        try {
            $connection = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            throw new \RuntimeException('Connection failed: ' . $e->getMessage());
        }

        return $connection;
    }
}
