<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Infrastructure;

use Deliverea\CoffeeMachine\Domain\Beverage\DrinkRepository;

final class MysqlDrinkRepository implements DrinkRepository
{
    private $connection;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function increaseMoney(string $drink, float $money): void
    {
        $statement_insert = sprintf("INSERT INTO product VALUES('%s', '%f') ", $drink, $money);
        $insertResult = $this->connection->query($statement_insert);

        if(!$insertResult) {
            $statement = sprintf("UPDATE product SET money = money + %f WHERE drink='%s'", $money, $drink);
            $this->connection->query($statement);
        }
    }
}
