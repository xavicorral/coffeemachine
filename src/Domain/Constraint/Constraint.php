<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Constraint;

interface Constraint
{
    public function check(): void;
}