<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Constraint;

use Deliverea\CoffeeMachine\Domain\Constraint\Constraint;

final class EnumConstraint implements Constraint
{
    const ALLOWED_SUGAR_VALUES = [0, 1, 2];

    private $allowedValues;
    private $param;

    public function __construct(array $allowedValues, $param)
    {
        $this->allowedValues = $allowedValues;
        $this->param = $param;
    }

    public function check(): void
    {
        if (!in_array($this->param, $this->allowedValues)) {
            throw new \RuntimeException(sprintf('The number of sugars should be between 0 and 2.'));
        };
    }
}
