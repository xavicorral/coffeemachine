<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Constraint;

use Deliverea\CoffeeMachine\Domain\Constraint\Constraint;

final class PriceConstraint implements Constraint
{
    const COFFEE_PRICE = 0.5;
    const TEA_PRICE = 0.4;
    const CHOCOLATE_PRICE = 0.6;

    private $minPrice;
    private $beverageName;
    private $introducedMoney;

    public function __construct(float $minPrice, string $beverageName, $introducedMoney)
    {
        $this->minPrice = $minPrice;
        $this->beverageName = $beverageName;
        $this->introducedMoney = $introducedMoney;
    }

    public function check(): void
    {
        if ((float)$this->introducedMoney < $this->minPrice) {
            throw new \RuntimeException(sprintf('The %s costs %.1f.', $this->beverageName, $this->minPrice));
        }
    }

    public function getMoney(): float
    {
        return (float)$this->introducedMoney;
    }
}
