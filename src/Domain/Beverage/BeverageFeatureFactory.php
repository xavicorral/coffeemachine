<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\Constraint\EnumConstraint;
use Deliverea\CoffeeMachine\Domain\Constraint\PriceConstraint;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;
use Deliverea\CoffeeMachine\Domain\TextTransformation\FeatureToTextTransformer;
use Deliverea\CoffeeMachine\Infrastructure\MysqlConnection;
use Deliverea\CoffeeMachine\Infrastructure\MysqlDrinkRepository;

final class BeverageFeatureFactory
{
    CONST DRINK_NOT_FOUND_MESSAGE = 'The drink type should be tea, coffee or chocolate';

    public static function create(string $beverageName, $featureParam, $baseBeverage = null)
    {
        switch ($beverageName) {
            case BeverageFeature::COFFEE_NAME:
                $beverageFeature = new DrinkFeature(
                    new FeatureToTextTransformer(BeverageToTextTransformer::PRODUCT_TEXT),
                    new PriceConstraint(PriceConstraint::COFFEE_PRICE, $beverageName, $featureParam),
                    BeverageFeature::COFFEE_NAME,
                    new MysqlDrinkRepository(MysqlConnection::create())
                );
                break;
            case BeverageFeature::TEA_NAME:
                $beverageFeature = new DrinkFeature(
                    new FeatureToTextTransformer(BeverageToTextTransformer::PRODUCT_TEXT),
                    new PriceConstraint(PriceConstraint::TEA_PRICE, $beverageName, $featureParam),
                    BeverageFeature::TEA_NAME,
                    new MysqlDrinkRepository(MysqlConnection::create())
                );
                break;
            case BeverageFeature::CHOCOLATE_NAME:
                $beverageFeature = new DrinkFeature(
                    new FeatureToTextTransformer(BeverageToTextTransformer::PRODUCT_TEXT),
                    new PriceConstraint(PriceConstraint::CHOCOLATE_PRICE, $beverageName, $featureParam),
                    BeverageFeature::CHOCOLATE_NAME,
                    new MysqlDrinkRepository(MysqlConnection::create())
                );
                break;
            case BeverageFeature::EXTRA_HOT_NAME:
                $beverageFeature = new ExtraHotFeatureDecorator(
                    new FeatureToTextTransformer(BeverageToTextTransformer::EXTRA_HOT_TEXT),
                    $baseBeverage,
                    (bool)$featureParam
                );
                break;
            case BeverageFeature::SUGAR_NAME:
                $beverageFeature = new SugarFeatureDecorator(
                    new FeatureToTextTransformer(BeverageToTextTransformer::SUGAR_TEXT),
                    new EnumConstraint(EnumConstraint::ALLOWED_SUGAR_VALUES, $featureParam),
                    $baseBeverage,
                    (string)$featureParam
                );
                break;
            default:
                throw new \RuntimeException(static::DRINK_NOT_FOUND_MESSAGE);
        }

        return $beverageFeature;
    }
}
