<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\Constraint\Constraint;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;

final class SugarFeatureDecorator implements BeverageFeature
{
    private $baseBeverage;
    private $textTransformer;
    private $constraint;
    private $sugars;

    public function __construct(BeverageToTextTransformer $textTransformer, Constraint $constraint, BeverageFeature $baseBeverage, string $sugars)
    {
        $this->textTransformer = $textTransformer;
        $this->baseBeverage = $baseBeverage;
        $this->constraint = $constraint;
        $this->sugars = $sugars;
    }

    public function getFeatureMessage(): string
    {
        $this->constraint->check();

        return $this->baseBeverage->getFeatureMessage() . ($this->sugars ? $this->textTransformer->getBeverageText($this->sugars) : '');
    }
}
