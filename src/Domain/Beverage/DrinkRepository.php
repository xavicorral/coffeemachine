<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

interface DrinkRepository
{
    public function increaseMoney(string $drink, float $money): void;
}