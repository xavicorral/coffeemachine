<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\Constraint\PriceConstraint;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;

final class DrinkFeature implements BeverageFeature
{
    private $textTransformer;
    private $constraint;
    private $drinkType;
    private $text;
    private $drinkRepository;

    public function __construct(BeverageToTextTransformer $textTransformer, PriceConstraint $constraint, string $drinkType, DrinkRepository $drinkRepository)
    {
        $this->textTransformer = $textTransformer;
        $this->constraint = $constraint;
        $this->drinkType = $drinkType;
        $this->drinkRepository = $drinkRepository;
    }

    public function getFeatureMessage(): string
    {
        $this->constraint->check();

        $this->drinkRepository->increaseMoney($this->drinkType, $this->constraint->getMoney());

        $this->text = $this->textTransformer->getBeverageText($this->drinkType);

        return $this->text;
    }
}
