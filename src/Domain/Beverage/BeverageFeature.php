<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

interface BeverageFeature
{
    const COFFEE_NAME = 'coffee';
    const TEA_NAME = 'tea';
    const CHOCOLATE_NAME = 'chocolate';
    const EXTRA_HOT_NAME = 'extra-hot';
    const SUGAR_NAME = 'sugars';

    public function getFeatureMessage(): string;
}