<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;

final class ExtraHotFeatureDecorator implements BeverageFeature
{
    private $baseBeverage;
    private $textTransformer;
    private $extraHot;

    public function __construct(BeverageToTextTransformer $textTransformer, BeverageFeature $baseBeverage, bool $extraHot)
    {
        $this->textTransformer = $textTransformer;
        $this->baseBeverage = $baseBeverage;
        $this->extraHot = $extraHot;
    }

    public function getFeatureMessage(): string
    {
        return $this->baseBeverage->getFeatureMessage() . ($this->extraHot ? $this->textTransformer->getBeverageText() : '');
    }
}
