<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\TextTransformation;

final class FeatureToTextTransformer implements BeverageToTextTransformer
{
    private $textPattern;

    public function __construct(string $textPattern)
    {
        $this->textPattern = $textPattern;
    }

    public function getBeverageText(string $param = null): string
    {
        return str_replace('%s', $param, $this->textPattern);
    }
}
