<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Domain\TextTransformation;

interface BeverageToTextTransformer
{
    const PRODUCT_TEXT = 'You have ordered a %s';
    const EXTRA_HOT_TEXT = ' extra hot';
    const SUGAR_TEXT = ' with %s sugars (stick included)';

    public function getBeverageText(string $param = null): string;
}