<?php

namespace Deliverea\CoffeeMachine\Console;

use Deliverea\CoffeeMachine\Domain\Beverage\BeverageFeature;
use Deliverea\CoffeeMachine\Domain\Beverage\BeverageFeatureFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeDrinkCommand extends Command
{
    protected static $defaultName = 'app:order-drink';

    protected function configure()
    {
        $this->addArgument(
            'drink-type',
            InputArgument::REQUIRED,
            'The type of the drink. (Tea, Coffee or Chocolate)'
        );

        $this->addArgument(
            'money',
            InputArgument::REQUIRED,
            'The amount of money given by the user'
        );

        $this->addArgument(
            'sugars',
            InputArgument::OPTIONAL,
            'The number of sugars you want. (0, 1, 2)',
            0
        );

        $this->addOption(
            'extra-hot',
            'e',
            InputOption::VALUE_NONE,
            $description = 'If the user wants to make the drink extra hot'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $drinkType = strtolower($input->getArgument('drink-type'));
        $money = $input->getArgument('money');
        $extraHot = $input->getOption('extra-hot');
        $sugars = $input->getArgument('sugars');

        try {
            $beverageFeatureFactory = new BeverageFeatureFactory();

            $drink = $beverageFeatureFactory::create($drinkType, $money);
            $extraHotFeatureDecorator = $beverageFeatureFactory::create(BeverageFeature::EXTRA_HOT_NAME, $extraHot, $drink);
            $sugarFeatureDecorator = $beverageFeatureFactory::create(BeverageFeature::SUGAR_NAME, $sugars, $extraHotFeatureDecorator);

            $output->writeln($sugarFeatureDecorator->getFeatureMessage());
        } catch (\RuntimeException $exception) {
            $output->writeln($exception->getMessage());
        }
    }
}
