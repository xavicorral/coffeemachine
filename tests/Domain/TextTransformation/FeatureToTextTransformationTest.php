<?php

declare(strict_types=1);

use Deliverea\CoffeeMachine\Domain\TextTransformation\FeatureToTextTransformer;
use PHPUnit\Framework\TestCase;

final class FeatureToTextTransformationTest extends TestCase
{
    /** @test */
    public function itShouldReplaceAPatternWithAGivenValue()
    {
        $handler = new FeatureToTextTransformer('This is the value to transform: %s');
        $this->assertEquals('This is the value to transform: NewValue', $handler->getBeverageText('NewValue'));
    }
}
