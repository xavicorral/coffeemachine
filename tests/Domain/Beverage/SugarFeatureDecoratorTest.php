<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Tests\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\Beverage\BeverageFeature;
use Deliverea\CoffeeMachine\Domain\Beverage\SugarFeatureDecorator;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;

final class SugarFeatureDecoratorTest extends BeverageFeatureUnitTestCase
{
    /** @test */
    public function itShouldAddAHotFeatureMessageToTheBaseFeature()
    {
        $baseBeverage = new class implements BeverageFeature {
            public function getFeatureMessage(): string
            {
                return 'You ordered a coffee';
            }
        };

        $textTransform = new class implements BeverageToTextTransformer {
            public function getBeverageText(string $param = null): string
            {
                return ' with 2 sugars (stick included)';
            }
        };

        $sugarFeatureDecorator = new SugarFeatureDecorator($textTransform, $this->passingConstraint, $baseBeverage, '2');
        $this->assertEquals('You ordered a coffee with 2 sugars (stick included)', $sugarFeatureDecorator->getFeatureMessage());
    }

}
