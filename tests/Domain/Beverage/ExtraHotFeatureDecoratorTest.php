<?php

declare(strict_types=1);

use Deliverea\CoffeeMachine\Domain\Beverage\BeverageFeature;
use Deliverea\CoffeeMachine\Domain\Beverage\ExtraHotFeatureDecorator;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;
use Deliverea\CoffeeMachine\Tests\Domain\Beverage\BeverageFeatureUnitTestCase;

final class ExtraHotFeatureDecoratorTest extends BeverageFeatureUnitTestCase
{
    /** @test */
    public function itShouldAddAHotFeatureMessageToTheBaseFeature()
    {
        $baseBeverage = new class implements BeverageFeature {
            public function getFeatureMessage(): string
            {
                return 'You ordered a coffee';
            }
        };

        $textTransform = new class implements BeverageToTextTransformer {
            public function getBeverageText(string $param = null): string
            {
                return ' extra hot';
            }
        };

        $extraHotFeatureDecorator = new ExtraHotFeatureDecorator($textTransform, $baseBeverage, true);
        $this->assertEquals('You ordered a coffee extra hot', $extraHotFeatureDecorator->getFeatureMessage());
    }
}
