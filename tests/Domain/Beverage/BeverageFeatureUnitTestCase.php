<?php

declare(strict_types=1);

namespace Deliverea\CoffeeMachine\Tests\Domain\Beverage;

use Deliverea\CoffeeMachine\Domain\Constraint\Constraint;
use Deliverea\CoffeeMachine\Domain\TextTransformation\BeverageToTextTransformer;
use PHPUnit\Framework\TestCase;

class BeverageFeatureUnitTestCase extends TestCase
{
    protected $textTransform;
    protected $failingConstraint;
    protected $passingConstraint;

    protected function setUp()
    {
        parent::setUp();

        $this->textTransform = new class implements BeverageToTextTransformer {
            public function getBeverageText(string $param = null): string
            {
                return 'You ordered a ' . $param;
            }
        };

        $this->failingConstraint  = new class implements Constraint {
            public function check(): void
            {
                throw new \RuntimeException('Something went wrong');
            }
        };

        $this->passingConstraint  = new class implements Constraint {
            public function check(): void
            {
                // do nothing
            }
        };
    }
}
