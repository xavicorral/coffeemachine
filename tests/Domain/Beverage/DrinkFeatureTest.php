<?php

declare(strict_types=1);

use Deliverea\CoffeeMachine\Domain\Beverage\DrinkFeature;
use Deliverea\CoffeeMachine\Domain\Beverage\DrinkRepository;
use Deliverea\CoffeeMachine\Domain\Constraint\PriceConstraint;
use Deliverea\CoffeeMachine\Tests\Domain\Beverage\BeverageFeatureUnitTestCase;

final class DrinkFeatureTest extends BeverageFeatureUnitTestCase
{
    private $drinkRepository;

    protected function setUp()
    {
        $this->drinkRepository = $this->createMock(DrinkRepository::class);
        return parent::setUp();
    }

    /** @test */
    public function itShouldAnExceptionWhenContraintIsNotAccomplished()
    {
        $this->expectException(\RuntimeException::class);
        $priceConstraint = new PriceConstraint(0.5, 'coffee', 0.4);

        $drinkFeature = new DrinkFeature($this->textTransform, $priceConstraint, 'coffee', $this->drinkRepository);
        $drinkFeature->getFeatureMessage();
    }

    /** @test */
    public function itShouldReturnAGivenTextWithAGivenParameter()
    {
        $priceConstraint = new PriceConstraint(0.5, 'coffee', 0.6);
        $this->drinkRepository
            ->expects($this->once())
            ->method('increaseMoney')
            ->willReturn(null);

        $drinkFeature = new DrinkFeature($this->textTransform, $priceConstraint, 'coffee', $this->drinkRepository);
        $this->assertEquals('You ordered a coffee', $drinkFeature->getFeatureMessage());
    }
}
