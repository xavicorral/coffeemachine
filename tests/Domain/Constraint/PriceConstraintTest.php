<?php

declare(strict_types=1);

use Deliverea\CoffeeMachine\Domain\Constraint\PriceConstraint;
use PHPUnit\Framework\TestCase;

final class PriceConstraintTest extends TestCase
{
    /** @test */
    public function isShouldThrowAnExceptionWhenTheGivenPriceIsUnderTheMinPrice()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('The coffee costs 5.0.');

        $constraint = new PriceConstraint(5, 'coffee',4);
        $constraint->check();
    }

    /** @test */
    public function isShouldNotThrowAnExceptionWhenTheGivenPriceIsAboveTheMinPrice()
    {
        $this->addToAssertionCount(1);

        $constraint = new PriceConstraint(5, 'coffee',6);
        $constraint->check();
    }
}
