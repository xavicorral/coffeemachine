use coffee_machine;

CREATE TABLE `product` (
  `drink` varchar(10) NOT NULL,
  `money` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`drink`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;